const Hook = require('../modules/hook');

const reactions = [
  {
    regExp: /.*(lj|isen).*/i,
    emoji: 'lj'
  },
  {
    regExp: /.*(benji|cool).*/i,
    emoji: 'benji'
  },
  {
    regExp: /.*(simon|snzy|chat).*/i,
    emoji: 'snzy'
  },
  {
    regExp: /.*(raytho|thomas).*/i,
    emoji: 'maot'
  },
  {
    regExp: /.*mania.*/i,
    emoji: 'mania'
  },
  {
    regExp: /.*(^|\s)ed(\s|$).*/i,
    emoji: 'ed'
  },
  {
    regExp: /.*(^|\s)(ah|ha)(!|\s|$).*/i,
    emoji: 'ha'
  },
  {
    regExp: /.*:ha:.*/i,
    emoji: 'ha'
  },
  {
    regExp: /.*arnau.*/i,
    emoji: 'kreygasm'
  },
  {
    regExp: /.*(^|\s)(travailler|bosser)(\s|$).*/i,
    emoji: 'bobross'
  },
  {
    regExp: /.*.*(^|\s)auchan(\s|$).*.*/i,
    emoji: 'masterrace'
  },
  {
    regExp: /.*(^|\s)aled(\s|$).*/i,
    emoji: 'aled'
  },
  {
    regExp: /.*(^|\s)(pug|ronpiche|orthographe)(\s|$).*/i,
    emoji: 'ronpiche'
  }
];

module.exports = new Hook.MessageHook({
  name: `Emojis`,
  desc: `Réagis à des message par des emojis`,

  onMessage: params => {
    let {msg} = params;

    reactions.forEach(reaction => {
      let emoji = msg.channel.guild.emojis.find(emoji => emoji.name === reaction.emoji);

      if (msg.content.search(reaction.regExp) !== -1) {
        msg.react(emoji);
      }
    });
  },

  userFilter: params => {
    let {bot, member} = params;
    return bot.user.id !== member.user.id;
  }
});
