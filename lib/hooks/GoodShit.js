const Hook = require('../modules/hook');

const regExp = /good shit/;
const goodShitText = '👌👀👌👀👌👀👌👀👌👀 good shit go౦ԁ sHit👌 thats ✔ some good👌👌shit right👌👌th 👌 ere👌👌👌 right✔there ✔✔if i do ƽaү so my selｆ 💯 i say so 💯 thats what im talking about right there right there (chorus: ʳᶦᵍʰᵗ ᵗʰᵉʳᵉ) mMMMMᎷМ💯 👌👌 👌НO0ОଠＯOOＯOОଠଠOoooᵒᵒᵒᵒᵒᵒᵒᵒᵒ👌 👌👌 👌 💯 👌 👀 👀 👀 👌👌Good shit';

module.exports = new Hook.MessageHook({
  name: `good shit`,
  desc: `Dites juste "good shit" au milieu d'une phrase! Une commande amusante pour toute la famille :)`,

  onMessage: params => {
    let {msg} = params;

    if (msg.content.search(regExp) !== -1) {
      msg.channel.sendMessage(goodShitText);
    }
  },

  userFilter: params => {
    let {bot, member} = params;
    return bot.user.id !== member.user.id;
  }
});
