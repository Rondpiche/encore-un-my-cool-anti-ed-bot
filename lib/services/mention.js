module.exports = {
  getUsersTargetedByMentions: function (client, mentions) {
    let targetedUsers = [];

    // Get users mentioned directly
    mentions.users.forEach(user => {
      targetedUsers.push(user);
    });

    // Get users mentioned by role
    mentions.roles.forEach(role => {
      role.members.forEach(member => {
        targetedUsers.push(member);
      });
    });

    return targetedUsers;
  }
};
