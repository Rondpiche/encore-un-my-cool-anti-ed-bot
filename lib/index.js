'use strict';

const fs = require('fs');
const cfg = require('../config');
const Discord = require('discord.js');
const Hook = require('./modules/hook');
const bot = new Discord.Client();
const winston = require('winston');
const moment = require('moment');

const hookApp = new Hook.HookApp({
  prefix: cfg.prefix,
  bot: bot
});

fs.readdirSync('./lib/hooks/').forEach(file => {
  hookApp.addHook(require(`./hooks/${file}`));
});

bot.login(cfg.token).then(() => {
  winston.log('info', 'Running !');
});

bot.on('disconnect', () => {
  winston.log('info', `Disconnected at ${moment()}`);
});

bot.on('reconnecting', () => {
  winston.log('info', `Attempting to reconnect at ${moment()}`);
});

