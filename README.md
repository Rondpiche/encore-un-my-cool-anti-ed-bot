# encore-un-my-cool-anti-ed-bot [![NPM version][npm-image]][npm-url]
> 

## Installation

Clone this repository, and run:
```sh
$ npm install
```

## Usage

```js
$ npm run bot
```
## License

BSD-3-Clause © [Mania]()


[npm-image]: https://badge.fury.io/js/encore-un-my-cool-anti-ed-bot.svg
[npm-url]: https://npmjs.org/package/encore-un-my-cool-anti-ed-bot
[daviddm-image]: https://david-dm.org//encore-un-my-cool-anti-ed-bot.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//encore-un-my-cool-anti-ed-bot
[coveralls-image]: https://coveralls.io/repos//encore-un-my-cool-anti-ed-bot/badge.svg
[coveralls-url]: https://coveralls.io/r//encore-un-my-cool-anti-ed-bot
